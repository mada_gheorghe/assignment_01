function distance(first, second) {
	if(!Array.isArray(first) || !Array.isArray(second)){
		throw new TypeError("InvalidType");
	}
	try{
		diff = first.filter(function(x) { return second.indexOf(x) < 0 });
		diff2 = second.filter(function(x) { return first.indexOf(x) < 0 });
		let diffF=diff.concat(diff2);
		let unique=[...new Set(diffF)];
		nr=unique.length;
		return nr;
	}
	catch(e) {
		console.error(e);
	}
}

module.exports.distance = distance